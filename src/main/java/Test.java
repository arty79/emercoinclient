import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

public class Test {
    public static void main(String[] args) throws IOException {
        HttpClient client = getHttpClient();
        requestPost(client, showFile("someFile.txt"));
        //requestPost(client, addFile("c:/readme.txt", 5));
        //requestPost(client, updateFile("c:/readme.txt", 5));
        //requestPost(client, deleteFile("readme.txt"));
    }

    private static HttpClient getHttpClient() {
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials
                = new UsernamePasswordCredentials("emccoinrpc", "pass");
        provider.setCredentials(AuthScope.ANY, credentials);

        return HttpClientBuilder.create()
                .setDefaultCredentialsProvider(provider)
                .build();
    }

    private static void requestPost(HttpClient client, JSONObject body) throws IOException {
        HttpPost post = new HttpPost("http://localhost:6662");
        StringEntity input = new StringEntity(body.toString());
        post.addHeader("content-type", "application/json");
        post.setEntity(input);
        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line = "";
        while ((line = rd.readLine()) != null) {
            System.out.println(line);
        }
    }

    private static JSONObject showFile(String filename) {
        JSONObject json = new JSONObject();
        json.put("method", "name_show");
        JSONArray params = new JSONArray();
        params.put(filename);
        json.put("params", params);
        return json;
    }

    private static JSONObject addFile(String filename, int days) throws IOException {
        Path path = Paths.get(filename);
        byte[] data = Files.readAllBytes(path);
        String value = Base64.encodeBase64String(data);
        JSONObject json = new JSONObject();
        json.put("method", "name_new");
        JSONArray params = new JSONArray();
        params.put(filename);
        params.put(value);
        params.put(days);
        json.put("params", params);
        return json;
    }

    private static JSONObject updateFile(String filename, int days) throws IOException {
        Path path = Paths.get(filename);
        byte[] data = Files.readAllBytes(path);
        String value = Base64.encodeBase64String(data);
        JSONObject json = new JSONObject();
        json.put("method", "name_update");
        JSONArray params = new JSONArray();
        params.put(filename);
        params.put(value);
        params.put(days);
        json.put("params", params);
        return json;
    }

    private static JSONObject deleteFile(String filename) {
        JSONObject json = new JSONObject();
        json.put("method", "name_delete");
        JSONArray params = new JSONArray();
        params.put(filename);
        json.put("params", params);
        return json;
    }
}